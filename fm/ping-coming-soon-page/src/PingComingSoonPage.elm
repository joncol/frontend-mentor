module PingComingSoonPage exposing (Model, Msg, init, main, update, view)

import Browser
import Html exposing (Html, div, form, h1, i, img, input, p, span, text)
import Html.Attributes exposing (class, placeholder, type_, value)
import Html.Events exposing (onInput, onSubmit)
import Validate exposing (Validator, ifBlank, ifInvalidEmail, validate)


main : Program () Model Msg
main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }


type alias Model =
    { email : String
    , userRegistered : Bool
    , validationErrors : List ValidationError
    , invalidEmailJustSubmitted : Bool
    }


type ValidationError
    = EmptyEmail
    | InvalidEmail


init : Model
init =
    let
        model =
            { email = ""
            , userRegistered = False
            , validationErrors = []
            , invalidEmailJustSubmitted = False
            }
    in
    { model | validationErrors = validationErrors model }


modelValidator : Validator ValidationError Model
modelValidator =
    Validate.firstError
        [ ifBlank .email EmptyEmail
        , ifInvalidEmail .email (always InvalidEmail)
        ]


validationErrors : Model -> List ValidationError
validationErrors model =
    case validate modelValidator model of
        Ok _ ->
            []

        Err errors ->
            errors


type Msg
    = ChangeEmail String
    | SubmitEmail


update : Msg -> Model -> Model
update msg model =
    case msg of
        ChangeEmail email ->
            { model
                | email = email
                , validationErrors = validationErrors { model | email = email }
                , invalidEmailJustSubmitted = False
            }

        SubmitEmail ->
            { model
                | userRegistered = List.isEmpty model.validationErrors
                , invalidEmailJustSubmitted = not <| List.isEmpty model.validationErrors
            }


view : Model -> Html Msg
view model =
    div [ class "flex h-screen" ]
        [ div
            [ class "fixed top-1/2 left-1/2 -mr-1/2 -translate-x-1/2 -translate-y-1/2 w-min p-6"
            , class "z-10 bg-ping-coming-soon-blue text-2xl"
            , class "text-white rounded-full"
            , class "bg-ping-coming-soon-page-blue"
            , class "font-semibold drop-shadow-[0_20px_20px_rgba(77,123,243,0.5)]"
            , class <|
                if model.userRegistered then
                    ""

                else
                    "hidden"
            ]
            [ text "Welcome!" ]
        , div
            [ class "flex flex-col justify-between items-center m-auto"
            , class "h-full max-h-[800px] w-[375px] sm:w-auto sm:max-w-2xl p-4"
            , class "font-libre-franklin font-light"
            , class "text-sm sm:text-base text-ping-coming-soon-page-very-dark-blue"
            , class <|
                if model.userRegistered then
                    "opacity-20"

                else
                    ""
            ]
            [ div [ class "mt-10" ] [ img [ class "h-5 logo" ] [] ]
            , h1 [ class "mt-10 text-xl sm:text-3xl" ]
                [ p []
                    [ span [ class "tracking-wider text-ping-coming-soon-page-gray" ]
                        [ text "We are launching " ]
                    , span
                        [ class "font-bold" ]
                        [ text "soon!" ]
                    ]
                ]
            , p [ class "mt-3" ] [ text "Subscribe and get notified" ]
            , form [ onSubmit SubmitEmail, class "mt-6 px-5 sm:px-0 w-full" ]
                [ div [ class "flex flex-col sm:flex-row w-full gap-4" ]
                    [ div [ class "flex-1 flex-col" ]
                        [ input
                            [ type_ "text"
                            , placeholder "Your email address..."
                            , value model.email
                            , onInput ChangeEmail
                            , class <|
                                if model.invalidEmailJustSubmitted then
                                    "border-ping-coming-soon-page-light-red"

                                else
                                    ""
                            ]
                            []
                        , div
                            [ class "mt-2 text-center sm:pl-10 sm:text-left text-xs italic"
                            , class "text-ping-coming-soon-page-light-red"
                            , class <|
                                if model.invalidEmailJustSubmitted then
                                    ""

                                else
                                    "hidden"
                            ]
                            [ text <|
                                if List.member EmptyEmail model.validationErrors then
                                    "Whoops! It looks like you forgot to add your email"

                                else if List.member InvalidEmail model.validationErrors then
                                    "Please provide a valid email address"

                                else
                                    ""
                            ]
                        ]
                    , div [ class "flex" ]
                        [ input
                            [ type_ "submit"
                            , value "Notify Me"
                            , class "w-full sm:w-[200px] py-3 sm:h-[44px] bg-ping-coming-soon-page-blue text-white"
                            , class "hover:bg-[rgb(113,149,245)]"
                            , class "rounded-full font-semibold"
                            , class "focus:outline-none"
                            , class "drop-shadow-xl drop-shadow-[0_5px_5px_rgba(77,123,243,0.4)]"
                            ]
                            []
                        ]
                    ]
                ]
            , img [ class "mt-6 illustration-dashboard" ] []
            , div [ class "flex flex-col items-center" ]
                [ div [ class "mt-20 flex flex-row gap-3 text-ping-coming-soon-page-blue" ]
                    [ div [ class "relative w-[32px] h-[32px] border-[1px] rounded-full" ]
                        [ div [ class "absolute inset-0 m-auto w-min h-min" ]
                            [ i
                                [ class "fa-brands fa-facebook-f"
                                , class "translate-y-[5%]"
                                ]
                                []
                            ]
                        ]
                    , div [ class "relative w-[32px] h-[32px] border-[1px] rounded-full" ]
                        [ div [ class "absolute inset-0 m-auto w-min h-min" ]
                            [ i
                                [ class "fa-brands fa-twitter"
                                , class "translate-y-[5%]"
                                ]
                                []
                            ]
                        ]
                    , div [ class "relative w-[32px] h-[32px] border-[1px] rounded-full" ]
                        [ div [ class "absolute inset-0 m-auto w-min h-min" ]
                            [ i
                                [ class "fa-brands fa-instagram"
                                , class "translate-y-[5%]"
                                ]
                                []
                            ]
                        ]
                    ]
                , div [ class "mt-6 mb-4 text-ping-coming-soon-page-gray" ]
                    [ text "© Copyright Ping. All rights reserved." ]
                ]
            ]
        ]
