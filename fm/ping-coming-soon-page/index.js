import { Elm } from "./src/PingComingSoonPage.elm";

const app = Elm.PingComingSoonPage.init({
  node: document.getElementById("root")
});

export default app;
