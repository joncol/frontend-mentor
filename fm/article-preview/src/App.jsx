import classNames from "classnames";
import React from "react";

import drawersImage from "../images/drawers.jpg";
import avatarMichelle from "../images/avatar-michelle.jpg";
import iconFacebook from "../images/icon-facebook.svg";
import iconTwitter from "../images/icon-twitter.svg";
import iconPinterest from "../images/icon-pinterest.svg";
import "../../style.css";

const FooterView = Object.freeze({
  AUTHOR_VIEW: "author_view",
  SHARE_VIEW: "share_view"
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      footerView: FooterView.AUTHOR_VIEW
    };

    this.handleArrowClicked = this.handleArrowClicked.bind(this);
  }

  handleArrowClicked() {
    this.setState((prevState) => ({
      footerView: prevState.footerView === FooterView.AUTHOR_VIEW ? FooterView.SHARE_VIEW : FooterView.AUTHOR_VIEW
    }));
  }

  render() {
    return (
      <div className="w-full h-screen">
        <div className="my-4 h-full">
          <div className="flex m-auto w-[375px] sm:w-[730px] h-full font-manrope font-medium text-[13px]">
            <div className="flex my-auto mx-[1.5rem] sm:mx-0 bg-white rounded-xl shadow-lg">
              <div className="flex flex-col sm:flex-row">
                <img
                  src={drawersImage}
                  alt=""
                  className={classNames(
                    "rounded-t-xl sm:rounded-none sm:rounded-l-xl",
                    "h-[200px] sm:h-[280px] sm:w-[285px] object-cover object-[left_25%]"
                  )}
                />
                <div className="flex flex-col justify-between">
                  <div className="px-9 pt-10 mb-5 sm:mb-0">
                    <h1 className="font-bold text-article-preview-very-dark-grayish-blue text-base sm:text-xl">
                      Shift the overall look and feel by adding these wonderful touches to furniture in your home
                    </h1>
                    <p className="mt-3 text-article-preview-desaturated-dark-blue">
                      Ever been in a room and felt like something was missing? Perhaps it felt slightly bare and
                      uninviting. I’ve got some simple tips to help you make any room feel complete.
                    </p>
                  </div>
                  <Footer view={this.state.footerView} onArrowClicked={this.handleArrowClicked} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function Footer({ view, onArrowClicked }) {
  let buttonBg;
  let buttonFill;
  if (view === FooterView.AUTHOR_VIEW) {
    buttonBg = "bg-article-preview-light-grayish-blue";
    buttonFill = "fill-[#6e8098]";
  } else if (view === FooterView.SHARE_VIEW) {
    buttonBg = "bg-article-preview-desaturated-dark-blue";
    buttonFill = "fill-white";
  }

  const authorViewOpacity = view === FooterView.AUTHOR_VIEW ? "opacity-100" : "opacity-0 sm:opacity-100";
  const shareViewOpacity = view === FooterView.SHARE_VIEW ? "opacity-100" : "opacity-0";

  return (
    <div className="relative h-[63px] sm:h-full">
      <div className="h-full">
        <div className={classNames("flex px-9 h-full transition-opacity duration-1000", authorViewOpacity)}>
          <div className="flex flex-row justify-between relative my-auto w-full">
            <div className="flex absolute h-full">
              <img
                src={avatarMichelle}
                alt="Profile picture of Michelle"
                className="object-contain max-h-full rounded-full"
              />
            </div>

            <div className="flex flex-col pl-14">
              <h2 className="font-bold text-[13px] text-article-preview-very-dark-grayish-blue">Michelle Appleton</h2>
              <p className="text-article-preview-grayish-blue">28 Jun 2020</p>
            </div>

            <button onClick={onArrowClicked} className={classNames("transition", "duration-1000", authorViewOpacity)}>
              <div
                className={classNames(
                  "flex p-2 bg-article-preview-light-grayish-blue rounded-full",
                  "transition",
                  "duration-1000",
                  buttonBg
                )}
              >
                <IconShare className={classNames("transition", "duration-1000", buttonFill)} />
              </div>
            </button>
          </div>
        </div>
      </div>

      {/* Speech bubble */}
      <div
        className={classNames(
          "absolute left-0 top-0 inline-flex w-full justify-between items-center py-3 px-9 h-[63px]",
          "sm:w-[248px] sm:h-[55px]",
          "bg-article-preview-very-dark-grayish-blue rounded-b-xl sm:rounded-xl",
          "transition-opacity duration-1000",
          shareViewOpacity,
          "sm:left-[270px]",
          "sm:-top-[58px]",
          "sm:shadow-xl",
          "sm:after:content-[''] sm:after:absolute sm:after:-bottom-[14px] sm:after:left-[109px]",
          "sm:after:border-[14px_14px_0]",
          "sm:after:border-y-article-preview-very-dark-grayish-blue sm:after:border-x-transparent"
        )}
      >
        <Share view={view} onArrowClicked={onArrowClicked} />

        <div className="ml-auto">
          <button onClick={onArrowClicked}>
            <div className="flex p-2 bg-article-preview-desaturated-dark-blue rounded-full sm:hidden">
              <IconShare className="fill-white" />
            </div>
          </button>
        </div>
      </div>
    </div>
  );
}

function IconShare(props) {
  return (
    <div className="w-[15px] h-[15px]">
      <svg width="15" height="15" {...props}>
        <path d="M15 6.495 8.766.014V3.88H7.441C3.33 3.88 0 7.039 0 10.936v2.049l.589-.612C2.59 10.294 5.422 9.11 8.39 9.11h.375v3.867L15 6.495z" />
      </svg>
    </div>
  );
}

function Share(props) {
  const { view, onArrowClicked } = props;
  const shareButtonsDisabled = view !== FooterView.SHARE_VIEW;
  return (
    <div className="inline-flex">
      <div className="pr-4">
        <h1 className="uppercase tracking-[0.35em] text-article-preview-grayish-blue">Share</h1>
      </div>

      <div className="inline-flex justify-self-end gap-4">
        <div className="flex">
          <button disabled={shareButtonsDisabled}>
            <img src={iconFacebook} alt="Facebook icon" />
          </button>
        </div>

        <div className="flex">
          <button disabled={shareButtonsDisabled}>
            <img src={iconTwitter} alt="Twitter icon" />
          </button>
        </div>

        <div className="flex">
          <button disabled={shareButtonsDisabled}>
            <img src={iconPinterest} alt="Pinterest icon" />
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
