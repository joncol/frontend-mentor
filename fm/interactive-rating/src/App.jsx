import classNames from "classnames";
import PropTypes from "prop-types";
import React from "react";

import iconStar from "../images/icon-star.svg";
import thankYouReceipt from "../images/illustration-thank-you.svg";
import "../../style.css";

const stageEnum = Object.freeze({
  voting: "voting",
  thanks: "thanks"
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stage: stageEnum.voting,
      rating: undefined
    };
  }

  static get propTypes() {
    return {
      stage: PropTypes.string,
      rating: PropTypes.number
    };
  }

  render() {
    const stagePage = {
      [stageEnum.voting]: (
        <VotingPage
          rating={this.state.rating}
          handleClickRating={(_e, rating) => this.setState({ rating })}
          handleSubmit={() => this.setState({ stage: stageEnum.thanks })}
        />
      ),
      [stageEnum.thanks]: <ThanksPage rating={this.state.rating} />
    };
    return stagePage[this.state.stage];
  }
}

function VotingPage({ rating, handleClickRating, handleSubmit }) {
  return (
    <div className="flex h-screen w-96 m-auto">
      <div
        className={classNames(
          "font-overpass",
          "text-[15px]",
          "text-interactive-rating-light-grey",
          "bg-interactive-rating-dark-blue",
          "bg-opacity-50",
          "rounded-2xl",
          "p-6",
          "my-auto",
          "mx-4"
        )}
      >
        <img
          src={iconStar}
          alt="Star"
          className={classNames(
            "bg-interactive-rating-dark-blue",
            "rounded-full",
            "w-10",
            "h-10",
            "p-3"
          )}
        />
        <h1
          className={classNames(
            "text-interactive-rating-white",
            "font-bold",
            "text-2xl",
            "my-4"
          )}
        >
          How did we do?
        </h1>
        <p className="pb-6">
          Please let us know how we did with your support request. All feedback
          is appreciated to help us improve our offering!
        </p>

        <RatingButtons rating={rating} handleClickRating={handleClickRating} />

        <SubmitButton rating={rating} handleSubmit={handleSubmit} />
      </div>
    </div>
  );
}

function RatingButtons({ rating, handleClickRating }) {
  const ratings = [...Array(5 + 1).keys()].slice(1);
  return (
    <div className={classNames("flex", "flex-row", "justify-between", "pb-5")}>
      {ratings.map(function (r) {
        const isSelected = r == rating;
        return (
          <button
            key={r}
            onClick={(e) => handleClickRating(e, r)}
            className={classNames(
              isSelected
                ? "bg-interactive-rating-medium-grey"
                : "bg-interactive-rating-dark-blue",
              "hover:bg-interactive-rating-orange",
              "hover:text-interactive-rating-white",
              isSelected ? "text-interactive-rating-white" : null,
              "rounded-full",
              "w-10",
              "h-10"
            )}
          >
            <div className="translate-y-[2px]">{r}</div>
          </button>
        );
      })}
    </div>
  );
}

function SubmitButton({ rating, handleSubmit }) {
  return (
    <button
      onClick={handleSubmit}
      className={classNames(
        "bg-interactive-rating-orange",
        "text-interactive-rating-white",
        "hover:text-interactive-rating-orange",
        "hover:bg-interactive-rating-white",
        "disabled:brightness-50",
        "disabled:bg-interactive-rating-orange",
        "disabled:text-interactive-rating-white",
        "uppercase",
        "tracking-widest",
        "text-sm",
        "font-bold",
        "py-3",
        "px-16",
        "w-full",
        "rounded-full"
      )}
      disabled={rating === undefined}
    >
      Submit
    </button>
  );
}

function ThanksPage({ rating }) {
  return (
    <div
      className={classNames(
        "flex",
        "h-screen",
        "w-96",
        "m-auto",
        "text-interactive-rating-light-grey",
        "font-overpass",
        "text-[15px]",
        "text-center"
      )}
    >
      <div
        className={classNames(
          "flex flex-col",
          "mx-4 my-auto",
          "bg-interactive-rating-dark-blue",
          "bg-opacity-50",
          "px-6",
          "py-8",
          "rounded-2xl"
        )}
      >
        <div className="self-center">
          <img src={thankYouReceipt} alt="Receipt" />
        </div>

        <div className="w-auto">
          <div
            className={classNames(
              "rounded-full",
              "w-max px-3 py-2 mx-auto my-6",
              "bg-interactive-rating-dark-blue",
              "text-interactive-rating-orange"
            )}
          >
            You selected {rating} out of 5
          </div>

          <h1
            className={classNames(
              "text-interactive-rating-white",
              "font-bold",
              "text-2xl",
              "my-4"
            )}
          >
            Thank you!
          </h1>
          <p>
            We appreciate you taking the time to give a rating. If you ever need
            more support, don’t hesitate to get in touch!
          </p>
        </div>
      </div>
    </div>
  );
}
export default App;
