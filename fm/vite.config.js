/* eslint-env node */

const path = require("path"),
  { resolve } = path;
import { defineConfig } from "vite";
const glob = require("glob");
import elmPlugin from "vite-plugin-elm";
import react from "@vitejs/plugin-react";

const indexFiles = glob.sync(resolve(__dirname) + "/**/index.html", {
  ignore: [
    resolve(__dirname, "index.html"),
    resolve(__dirname) + "/**/dist/**",
    resolve(__dirname + "/**/node_modules/**")
  ]
});

const inputFiles = indexFiles.reduce(function (acc, filename) {
  let newAcc = Object.assign({}, acc);
  let relPath = path.relative(resolve(__dirname), filename);
  newAcc[path.parse(relPath).dir] = filename;
  return newAcc;
}, {});

module.exports = defineConfig({
  build: {
    rollupOptions: {
      input: Object.assign(
        {
          main: resolve(__dirname, "index.html")
        },
        inputFiles
      )
    }
  },

  plugins: [elmPlugin(), react()]
});
