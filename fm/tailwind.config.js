module.exports = {
  content: ["./**/index.html", "./*/src/**/*.{elm,js,ts,jsx,tsx}"],

  theme: {
    extend: {
      fontFamily: {
        outfit: ["Outfit", "sans-serif"],
        "red-hat-display": ["Red Hat Display", "sans-serif"],
        inter: ["Inter", "sans-serif"],
        "lexend-deca": ["Lexend Deca", "sans-serif"],
        "big-shoulders-display": ["Big Shoulders Display", "cursive"],
        "kumbh-sans": ["Kumbh Sans", "sans-serif"],
        overpass: ["Overpass", "sans-serif"],
        spartan: ["Spartan", "sans-serif"],
        manrope: ["Manrope", "sans-serif"],
        poppins: ["Poppins", "sans-serif"],
        "josefin-sans": ["Josefin Sans", "sans-serif"],
        karla: ["Karla", "sans-serif"],
        "libre-franklin": ["Libre Franklin", "sans-serif"],
        "open-sans": ["Open Sans", "sans-serif"],
        rubik: ["Rubik", "sans-serif"]
      },

      transitionDuration: {
        DEFAULT: "150ms"
      },

      colors: {
        // qr-code
        "light-gray": "hsl(212, 45%, 89%)",
        "grayish-blue": "hsl(220, 15%, 55%)",
        "dark-blue": "hsl(218, 44%, 22%)",

        // nft-preview-card
        "soft-blue": "hsl(215, 51%, 70%)",
        cyan: "hsl(178, 100%, 50%)",
        "very-dark-blue1": "hsl(217, 54%, 11%)", // main background
        "very-dark-blue2": "hsl(216, 50%, 16%)", // card background
        "very-dark-blue3": "hsl(215, 32%, 27%)", // line

        "order-summary": {
          "pale-blue": "hsl(225, 100%, 94%)",
          "bright-blue": "hsl(245, 75%, 52%)",
          "very-pale-blue": "hsl(225, 100%, 98%)",
          "desaturated-blue": "hsl(224, 23%, 55%)",
          "dark-blue": "hsl(223, 47%, 23%)",
          purple: "rgba(56, 41, 224, 0.75)"
        },

        "stats-preview-card": {
          "very-dark-blue": "hsl(233, 47%, 7%)", // main background
          "dark-desaturated-blue": "hsl(244, 38%, 16%)", // card background
          "soft-violet": "hsl(277, 64%, 61%)", // accent
          white: "hsl(0, 0%, 100%)", // main heading, stats
          "slightly-transparent-white-main-paragraph":
            "hsla(0, 0%, 100%, 0.75)", // main paragraph
          "slightly-transparent-white-stat-headings": "hsla(0, 0%, 100%, 0.6)" // stat headings
        },

        "3-column-preview-card": {
          "bright-orange": "hsl(31, 77%, 52%)",
          "dark-cyan": "hsl(184, 100%, 22%)",
          "very-dark-cyan": "hsl(179, 100%, 13%)",
          "transparent-white": "hsla(0, 0%, 100%, 0.75)", // paragraphs
          "very-light-gray": "hsl(0, 0%, 95%)" // background, headings, buttons
        },

        "profile-card": {
          "dark-cyan": "hsl(185, 75%, 39%)",
          "very-dark-desaturated-blue": "hsl(229, 23%, 23%)",
          "dark-grayish-blue": "hsl(227, 10%, 46%)",
          "dark-gray": "hsl(0, 0%, 59%)"
        },

        "faq-accordion-card": {
          "very-dark-desaturated-blue": "hsl(238, 29%, 16%)", // text
          "soft-red": "hsl(14, 88%, 65%)", // text
          "soft-violet": "hsl(273, 75%, 66%)", // background gradient
          "soft-blue": "hsl(240, 73%, 65%)", // background gradient
          "very-dark-grayish-blue": "hsl(237, 12%, 33%)", // text
          "dark-grayish-blue": "hsl(240, 6%, 50%)", // text
          "light-grayish-blue": "hsl(240, 5%, 91%)" // dividers
        },

        "interactive-rating": {
          orange: "hsl(25, 97%, 53%)", // primary
          white: "hsl(0, 0%, 100%)",
          "light-grey": "hsl(217, 12%, 63%)",
          "medium-grey": "hsl(216, 12%, 54%)",
          "dark-blue": "hsl(213, 19%, 18%)",
          "very-dark-blue": "hsl(216, 12%, 8%)"
        },

        "social-proof-section": {
          "very-dark-magenta": "hsl(300, 43%, 22%)",
          "soft-pink": "hsl(333, 80%, 67%)",
          "dark-grayish-magenta": "hsl(303, 10%, 53%)",
          "light-grayish-magenta": "hsl(300, 24%, 96%)",
          white: "hsl(0, 0%, 100%)"
        },

        "article-preview": {
          "very-dark-grayish-blue": "hsl(217, 19%, 35%)",
          "desaturated-dark-blue": "hsl(214, 17%, 51%)",
          "grayish-blue": "hsl(212, 23%, 69%)",
          "light-grayish-blue": "hsl(210, 46%, 95%)"
        },

        "four-card-feature-section": {
          red: "hsl(0, 78%, 62%)",
          cyan: "hsl(180, 62%, 55%)",
          orange: "hsl(34, 97%, 64%)",
          blue: "hsl(212, 86%, 64%)",
          "very-dark-blue": "hsl(234, 12%, 34%)",
          "grayish-blue": "hsl(229, 6%, 66%)",
          "very-light-gray": "hsl(0, 0%, 98%)"
        },

        "base-apparel-coming-soon": {
          "desaturated-red": "hsl(0, 36%, 70%)",
          "soft-red": "hsl(0, 93%, 68%)",
          "dark-grayish-red": "hsl(0, 6%, 24%)"
        },

        "intro-component-with-signup-form": {
          red: "hsl(0, 100%, 74%)",
          green: "hsl(154, 59%, 51%)",
          blue: "hsl(248, 32%, 49%)",
          "dark-blue": "hsl(249, 10%, 26%)",
          "grayish-blue": "hsl(246, 25%, 77%)"
        },

        "single-price-grid-component": {
          cyan: "hsl(179, 62%, 43%)",
          "bright-yellow": "hsl(71, 73%, 54%)",
          "light-gray": "hsl(204, 43%, 93%)",
          "grayish-blue": "hsl(218, 22%, 67%)"
        },

        "ping-coming-soon-page": {
          blue: "hsl(223, 87%, 63%)",
          "pale-blue": "hsl(223, 100%, 88%)",
          "light-red": "hsl(354, 100%, 66%)",
          gray: "hsl(0, 0%, 59%)",
          "very-dark-blue": "hsl(209, 33%, 12%)"
        },

        "huddle-landing-page": {
          violet: "hsl(257, 40%, 49%)",
          "soft-magenta": "hsl(300, 69%, 71%)"
        },

        "advice-generator-app": {
          "light-cyan": "hsl(193, 38%, 86%)",
          "neon-green": "hsl(150, 100%, 66%)",
          "grayish-blue": "hsl(217, 19%, 38%)",
          "dark-grayish-blue": "hsl(217, 19%, 24%)",
          "dark-blue": "hsl(218, 23%, 16%)"
        },

        "time-tracking-dashboard": {
          blue: "hsl(246, 80%, 60%)",
          work: { "light-red": "hsl(15, 100%, 70%)" },
          play: { "soft-blue": "hsl(195, 74%, 62%)" },
          study: { "light-red": "hsl(348, 100%, 68%)" },
          exercise: { "lime-green": "hsl(145, 58%, 55%)" },
          social: { violet: "hsl(264, 64%, 52%)" },
          "self-care": { "soft-orange": "hsl(43, 84%, 65%)" },
          "very-dark-blue": "hsl(226, 43%, 10%)",
          "dark-blue": "hsl(235, 46%, 20%)",
          "desaturated-blue": "hsl(235, 45%, 61%)",
          "pale-blue": "hsl(236, 100%, 87%)"
        }
      }
    }
  },
  plugins: [require("@tailwindcss/forms")]
};
