import { Elm } from "./src/IntroComponent.elm";

const app = Elm.IntroComponent.init({
  node: document.getElementById("root")
});

export default app;
