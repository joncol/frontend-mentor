((html-mode . ((eval . (progn (lsp-deferred)
                              (display-fill-column-indicator-mode -1)))
               (fill-column . 120)))

 (js-jsx-mode . ((fill-column . 120)))

 (css-mode . ((eval . (display-fill-column-indicator-mode -1))
              (fill-column . 120))))
